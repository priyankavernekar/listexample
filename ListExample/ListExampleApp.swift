//
//  ListExampleApp.swift
//  ListExample
//
//  Created by Priyanka Vernekar on 11/23/21.
//

import SwiftUI

@main
struct ListExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
