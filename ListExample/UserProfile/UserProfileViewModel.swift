//
//  UserProfileViewModel.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import Foundation

/**
 View Model for Profile
 */
final class UserProfileViewModel: JsonDecoder, ObservableObject {
    
    var datasource: Profile? {
        
        if let profileData = self.dataFromJsonFile(fileName: "profile-view") {
            do {
                if let profileModel = try self.decode(model: Profile.self, from: profileData) as? Profile {
                    return profileModel
                }
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
