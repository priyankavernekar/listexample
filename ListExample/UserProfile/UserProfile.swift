//
//  UserProfile.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import Foundation

struct Profile: Codable {
    var title: String?
    var section: [ProfileSection]?
}

struct ProfileSection: Codable, Hashable {
    var sectionTitle: String?
    var type: String = CellType.regular.rawValue
    var rows: [ProfileRow]?
}

struct ProfileRow: Codable, Hashable {
    var rowName: String?
    var rowType: String = ButtonType.clear.rawValue
    var textAlignment: String = "left"
    
}
