//
//  UserProfileView.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import SwiftUI

enum TableSections {
    case disclosure
    case button
}

struct UserProfileView: View, JsonDecoder {
    
    var viewModel = UserProfileViewModel()
    
    var body: some View {
            VStack(alignment: .leading) {
                Spacer()
                Text(viewModel.datasource?.title?.capitalized ?? "")
                    .font(Font.largeTitle)
                    .fontWeight(.bold)
                    .padding(.leading, 25)
                    .padding(.top, 20)
                
                List {
                    ForEach(viewModel.datasource?.section ?? [ProfileSection()], id: \.self) { section in
                        Section(header: Text(section.sectionTitle ?? "")) {
                            ForEach((section.rows ?? [ProfileRow()]).indices, id: \.self) { index in
                                self.cellForRow(for: section, index: index)
                            }
                        }
                    }
                }
                
            }.background(Color(UIColor.systemGroupedBackground))
        }
    
    func cellForRow(for sectionObject: ProfileSection, index: Int) -> AnyView {
        guard let rows = sectionObject.rows  else {
            return AnyView(Color.black)
        }
        let profileRow = rows[index]
        if sectionObject.type == CellType.disclosure.rawValue {
            return AnyView(DisclosureCell(labelText: profileRow.rowName ?? "",
                                          destinationView: AnyView(UserProfileDetailView())))
        } else {
            let buttonTheme = ButtonType.buttonTheme(type: ButtonType(rawValue: profileRow.rowType) ?? .clear)
            return AnyView(ClearButton(labelText: profileRow.rowName ?? "",
                                       labelTextColor: buttonTheme.labelTextColor,
                                       labelFont: buttonTheme.labelFont,
                                       labelTextAlignment: buttonTheme.labelTextAlignment,
                                       destinationView: AnyView(UserProfileDetailView())))
        }
    }
    
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        UserProfileView()
    }
}
