//
//  UserProfileDetailView.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import SwiftUI

/**
 View to display Profile Details
 */
struct UserProfileDetailView: View {
    
    var body: some View {
        Text("Profile Detail View")
    }
}

struct UserProfileDetailView_Previews: PreviewProvider {
    static var previews: some View {
        UserProfileDetailView()
    }
}
