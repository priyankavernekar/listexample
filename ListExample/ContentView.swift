//
//  ContentView.swift
//  ListExample
//
//  Created by Priyanka Vernekar on 11/23/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                ZStack {
                    Color(red: 15 / 255, green: 46 / 255, blue: 128 / 255)
                        .ignoresSafeArea()
                    NavigationLink(destination: UserProfileView()) {
                        Text("My Profile")
                            .frame(width: geometry.size.width * 0.7, height: 30)
                            .padding()
                            .foregroundColor(Color.blue)
                            .background(Color.white)
                            .cornerRadius(10)
                            .font(Font.title)
                    }
                }.accentColor(Color.white)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
