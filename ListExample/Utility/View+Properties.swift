//
//  ViewProperties.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

public enum CellType: String, Codable {
    case regular
    case disclosure
    case button
}

public enum ButtonType: String, Codable {
    case clear
    case delete
    case solid
    
    static func buttonTheme(type: ButtonType) -> ThemeProperty {
        switch type {
        case .clear:
            return ThemeProperty(labelTextColor: Color.blue,
                                 labelFont: Font.system(size: 18, weight: .regular),
                                 labelTextAlignment: .center, backgroundColor: .white)
        case .delete:
            return ThemeProperty(labelTextColor: Color.red,
                                 labelFont: Font.system(size: 18, weight: .regular),
                                 labelTextAlignment: .center, backgroundColor: .white)
        case .solid:
            return ThemeProperty(labelTextColor: .white,
                                 labelFont: Font.system(size: 18, weight: .regular),
                                 labelTextAlignment: .center, backgroundColor: Color.blue)
        }
    }
}

struct ThemeProperty {
    var labelTextColor: Color
    var labelFont: Font
    var labelTextAlignment: Alignment
    var backgroundColor: Color
}

struct LabelTextAlignment {
    
    static func textAlignment(valueString: String) -> Alignment {
        switch valueString {
        case "left": return Alignment.leading
        case "right": return Alignment.trailing
        case "center": return Alignment.center
        default: return Alignment.leading
        }
    }
}
