//
//  JsonDecoder.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import Foundation

protocol JsonDecoder {
    
}

extension JsonDecoder {
    
    func dataFromJsonFile(fileName: String) -> Data? {
        
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let fileData = try Data(contentsOf: url)
                return fileData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    func decode<T>(model: T.Type, from data: Data) throws -> T where T: Decodable {
        
        guard let modelStruct = try? model.init(jsonData: data) else {
            // swiftlint:disable force_cast
            return 0 as! T
            // swiftlint:enable force_cast
        }
        return modelStruct
    }
}

extension Decodable {
    init(jsonData: Data) throws {
        self = try JSONDecoder().decode(Self.self, from: jsonData)
    }
}
