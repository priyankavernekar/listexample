//
//  DisclosureCell.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import SwiftUI

struct DisclosureCell: View {
    
    var labelText: String
    /// Setting default color to black, but this can be configured from calling context
    var labelTextColor = Color.black
    /// Setting default Font, but this can be configured from calling context
    var labelFont = Font.system(size: 18, weight: .regular, design: .default)
    var destinationView = AnyView(Color.white)
    
    var body: some View {
        NavigationLink(destination: destinationView) {
            Text(labelText)
                .foregroundColor(labelTextColor)
                .font(labelFont)
        }
    }
}

struct DisclosureCell_Previews: PreviewProvider {
    static var previews: some View {
        DisclosureCell(labelText: "")
    }
}
