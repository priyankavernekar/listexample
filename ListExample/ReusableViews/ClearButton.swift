//
//  ClearButton.swift
//  cph-patient-companion
//
//  Copyright © 2021 ConvergeHEALTH by Deloitte. All rights reserved.
//

import SwiftUI

struct ClearButton: View {
    
    var labelText: String
    /// Setting default color to black, but this can be configured from calling context
    var labelTextColor = Color.blue
    /// Setting default Font, but this can be configured from calling context
    var labelFont = Font.system(size: 18, weight: .regular, design: .default)
    var labelTextAlignment = Alignment.center
    var destinationView = AnyView(Color.white)
    
    var body: some View {
        Button(action: {
            print("Button tapped!")
        }) {
            Text(labelText)
                .frame(maxWidth: .infinity, alignment: labelTextAlignment)
                .font(labelFont)
            .foregroundColor(labelTextColor)
            .background(Color.clear)
        }
    }
}

struct ClearButton_Previews: PreviewProvider {
    static var previews: some View {
        ClearButton(labelText: "")
    }
}
